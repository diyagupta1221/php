FROM php
COPY ./index.php ./
COPY ./functions.php ./
EXPOSE 80
CMD ["php","-S","0.0.0.0:80"]
